<?php


class News_m extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll(){
		return $this->db
				->select("*")
				->from("berita")
				->order_by("tgl_post", "desc")
				->get()->result_array();
	}

	public function getWhere($id){
		return $this->db->where('id', $id)->get('berita')->row();
	}

	public function read($id){
		$get = $this->db->get('berita')->row();
		$views = (int)$get->views + 1;
		$this->db->where('id', $id)->update('berita', array('views' => $views));
	}

	public function save($data){
		return $this->db->insert('berita', $data);
	}

	public function update($data, $id){
		return $this->db->update('berita', $data, array('id' => $id));
	}

	public function hapus($id){
		return $this->db->delete('berita', array('id' => $id));
	}
}
