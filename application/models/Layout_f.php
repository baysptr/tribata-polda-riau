<?php


class Layout_f extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function meta(){
		$html = '<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<meta http-equiv="X-UA-Compatible" content="ie=edge">
				<link rel="stylesheet" href="'. base_url() .'assets_frontend/css/style.css">
				<link rel="stylesheet" href="'. base_url() .'assets_frontend/vendor/fontawesome/css/all.css">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
						<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
						<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
						<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
						<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
				<title>TribrataNews Riau</title>';
		return $html;
	}

	public function header($terkinis){
		$link = "";
		foreach ($terkinis as $terkini){
			$link = $link . "<a href='".site_url()."read/news/".$terkini['id']."/".url_title($terkini['judul'], 'dash', true)."' style='color: white'>".$terkini['judul']."</a>&nbsp;|&nbsp;";
		}
		$html = '<header>
        <!-- NAVIGATION SIDEBAR MENU -->
        <section class="wrapper">
            <nav id="sidebar">
                <div id="dismiss">
                    <i class="fas fa-arrow-left"></i>
                </div>

                <div class="sidebar-header">
                    <!-- <h3>Bootstrap Sidebar</h3> -->
                </div>

                <ul class="list-unstyled components">
                    <li class="active">
                        <a href="#">Headline</a>
                    </li>
                    <li>
                        <a href="#profilSubMenu" data-toggle="collapse" aria-expanded="false"
                            class="dropdown-toggle">Profil</a>
                        <ul class="collapse list-unstyled" id="profilSubMenu">
                            <li>
                                <a href="'.site_url().'kata_sambutan">Kata Sambutan</a>
                            </li>
                            <li>
                                <a href="'.site_url().'profile">Profile</a>
                            </li>
                            <li>
                                <a href="'.site_url().'visi_misi">Visi & Misi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#jajaranSubMenu" data-toggle="collapse" aria-expanded="false"
                            class="dropdown-toggle">Jajaran</a>
                        <ul class="collapse list-unstyled" id="jajaranSubMenu">
                            <li>
                                <a href="#">Polres Riau</a>
                            </li>
                            <li>
                                <a href="#">Kapolres Balerang</a>
                            </li>
                            <li>
                                <a href="#">Kapolres Tanjung Pinang</a>
                            </li>
                            <li>
                                <a href="#">Kapolres Karimun</a>
                            </li>
                            <li>
                                <a href="#">Kapolres Natuna</a>
                            </li>
                            <li>
                                <a href="#">Kapolres Bintan</a>
                            </li>
                            <li>
                                <a href="#">Kapolres Lingga</a>
                            </li>
                            <li>
                                <a href="#">Kapolres Kepulauan Anambas</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#direktoratSubMenu" data-toggle="collapse" aria-expanded="false"
                            class="dropdown-toggle">Direktorat</a>
                        <ul class="collapse list-unstyled" id="direktoratSubMenu">
                            <li>
                                <a href="'.site_url().'binkam">Dit Binkam</a>
                            </li>
                            <li>
                                <a href="'.site_url().'binmas">Dit Binmas</a>
                            </li>
                            <li>
                                <a href="'.site_url().'sabhara">Dit Sabhara</a>
                            </li>
                            <li>
                                <a href="'.site_url().'brimob">Brimob</a>
                            </li>
                            <li>
                                <a href="'.site_url().'polair">Polair</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#reskrimSubMenu" data-toggle="collapse" aria-expanded="false"
                            class="dropdown-toggle">Reskrim</a>
                        <ul class="collapse list-unstyled" id="reskrimSubMenu">
                            <li>
                                <a href="'.site_url().'reskrim_um">Reskrim Um</a>
                            </li>
                            <li>
                                <a href="'.site_url().'reskrim_sus">Reskrim Sus</a>
                            </li>
                            <li>
                                <a href="'.site_url().'res_narkoba">Res Narkoba</a>
                            </li>
                            <li>
                                <a href="'.site_url().'res_cyber_media">Reskrim Cyber Media</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="'.site_url().'lantas">Lantas</a>
                    </li>
                    <li>
                        <a href="'.site_url().'mitra_polisi">Mitra Polisi</a>
                    </li>
                    <li>
                        <a href="#giatSubmenu" data-toggle="collapse" aria-expanded="false"
                            class="dropdown-toggle">Pages</a>
                        <ul class="collapse list-unstyled" id="giatSubmenu">
                            <li>
                                <a href="'.site_url().'giat_ops">Giat Ops</a>
                            </li>
                            <li>
                                <a href="'.site_url().'giat_administrasi">Giat Administrasi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Foto</a>
                    </li>
                    <li>
                        <a href="#">Opini</a>
                    </li>
                </ul>
            </nav>
        </section>

        <div class="overlay"></div>
        <!-- END NAVIGATION SIDEBAR MENU -->

        <!-- header Top -->
        <section>
            <nav class="navbar navbar-expand-md bg-header">
                <div class="container custom-container">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-2 col-md-1 mt-1">
                                <button type="button" id="sidebarCollapse" class="btn btn-outline-light toggle-sidebar">
                                    <i class="fas fa-bars"></i>
                            </div>
                            <div class="col-10 col-md-11">
                                <div class="row">
                                    <div class="col-md-2 align-items-center text-center">
                                        <h5><span class=" badge btn-red">Headline</span></h5>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="font-white m-0">
                                            <marquee>'.$link.'</marquee>
                                        </div>
                                        <!-- <div class="font-white m-0">
                                            <p>test</p>
                                        </div>
                                        <div class="font-white m-0">
                                            <p>test</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md align-items-center text-center">
                                <button type="button" class="btn btn-outline-light"><i
                                        class="fas fa-angle-left font-white"></i></button>
                                <button type="button" class="btn btn-outline-light"><i
                                        class="fas fa-angle-right font-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
            </nav>
        </section>
        <!-- end header Top -->

        <!-- header logo & search -->
        <section>
            <div class="container custom-container my-3">
                <div class="row justify-content-between">
                    <div class="col-sm-4">
                        <a href="'.base_url().'"><img src="'. base_url() .'assets_frontend/img/logotribratanewsriauall.png" class="logo-style" alt="logo" /></a>
                    </div>
                    <div class="col-sm-4 float-right align-self-center">
                        <div class="input-group search">
                            <input type="text" class="form-control" placeholder="Search" aria-label="Search"
                                aria-describedby="search">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="search">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end header logo & search -->

        <!-- menu bar -->
        <section class="navbar-menu">
            <div class="container custom-container my-3">

                <ul class="nav justify-content-between">
                    <li class="nav-item">
                        <a class="nav-link active font-blackBold" href="#">Headline</a>
                    </li>
                    <li class="nav-item dropdown font-blackBold">
                        <a class="nav-link dropdown-toggle font-blackBold" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="true" aria-expanded="false">Profil</a>
                        <div class="dropdown-menu dropdown-border">
                            <a class="dropdown-item" href="'.site_url().'kata_sambutan">Kata Sambutan</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'profile">Profil</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'visi_misi">Visi & Misi</a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    <li class="nav-item dropdown font-blackBold">
                        <a class="nav-link dropdown-toggle font-blackBold" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="true" aria-expanded="false">Jajaran</a>
                        <div class="dropdown-menu dropdown-border">
                            <a class="dropdown-item" href="#">Polres Bengkalis</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Dumai</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Indragiri Hilir</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Indragiri Hulu</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Kampar</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Kuantan Singingi</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Pelalawan</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Rokan Hilir</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Rokan Hulu</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polres Siak</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polresta Pekanbaru</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Polresta Kepulauan Meranti</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown font-blackBold">
                        <a class="nav-link dropdown-toggle font-blackBold" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="true" aria-expanded="false">Direktorat</a>
                        <div class="dropdown-menu dropdown-border">
                            <a class="dropdown-item" href="'.site_url().'binkam">Dit Binkam</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'binmas">Dit Bimnas</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'sabhara">Dit Sabhara</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'brimob">Brimob</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'polair">Polair</a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    <li class="nav-item dropdown font-blackBold">
                        <a class="nav-link dropdown-toggle font-blackBold" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="true" aria-expanded="false">Reskrim</a>
                        <div class="dropdown-menu dropdown-border">
                            <a class="dropdown-item" href="'.site_url().'reskrim_um">Reskrim Um</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'reskrim_sus">Reskrim Sus</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'res_narkoba">Res Narkoba</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'res_cyber_media">Reskrim Cyber Media</a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link font-blackBold" href="'.site_url().'lantas">Lantas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link font-blackBold" href="'.site_url().'mitra_polisi">Mitra Polisi</a>
                    </li>
                    <li class="nav-item dropdown font-blackBold">
                        <a class="nav-link dropdown-toggle font-blackBold" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="true" aria-expanded="false">Giat</a>
                        <div class="dropdown-menu dropdown-border">
                            <a class="dropdown-item" href="'.site_url().'giat_ops">Giat Ops</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="'.site_url().'giat_administrasi">Giat Administrasi</a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link font-blackBold" href="#">Foto</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link font-blackBold" href="#">Opini</a>
                    </li>
                </ul>
            </div>
        </section>
        <!-- end menu bar -->

    </header>';
		return $html;
	}

	public function medsos(){
		$html = '<section class="bg-gray py-4">
					<div class="container custom-container">
						<div class="row justify-content-md-center">
							<div class="mx-3">
								<img src="'. base_url() .'assets_frontend/img/medsos/tw.png" alt="twitter" class="mx-2">
								<h5 class="text-center my-3">@TribrataRiau</h5>
							</div>
							<div class="mx-3">
								<img src="'. base_url() .'assets_frontend/img/medsos/ig.png" alt="instagram" class="mx-2">
								<h5 class="text-center my-3">@TribrataRiau</h5>
							</div>
							<div class="mx-3">
								<img src="'. base_url() .'assets_frontend/img/medsos/fb.png" alt="facebook" class="mx-2">
								<h5 class="text-center my-3">@TribrataRiau</h5>
							</div>
							<div class="mx-3">
								<img src="'. base_url() .'assets_frontend/img/medsos/yt.png" alt="youtube" class="mx-2">
								<h5 class="text-center my-3">@TribrataRiau</h5>
							</div>
						</div>
					</div>
				</section>';
		return $html;
	}

	public function footer(){
		$html = '<footer>
					<section class="bg-botblue py-4">
						<!-- <div class="float-right mx-3">
							<button href="#top" class="btn btn-success btn-circle btn-circle-lg"><i
									class="fas fa-angle-up"></i></button>
						</div> -->
						<div class="container custom-container">
							<div class="row font-white">
								<div class="col-md">
									<h5>BERIKAN KOMENTAR & SARAN</h5>
									<form>
										<div class="form-group">
											<label for="exampleFormControlTextarea1">Komentar</label>
											<textarea class="form-control form-control-sm" id="exampleFormControlTextarea1"
												rows="3"></textarea>
										</div>
										<div class="form-row">
											<div class="col">
												<label>Nickname</label>
												<input type="text" class="form-control form-control-sm">
											</div>
											<div class="col">
												<label>Email</label>
												<input type="text" class="form-control form-control-sm">
											</div>
										</div>
										<button type="submit" class="btn btn-primary btn-sm mt-3">Submit</button>
									</form>
								</div>
								<div class="vertical-devider"></div class="vertical-devider">
								<div class="col-md">
									<h5><span class="badge badge-light">INFORMASI</span></h5>
									<div class="row text-center">
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Polres Riau</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Visi & Misi</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Reskrim</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Mitra Polisi</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Giat Ops</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Lantas</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Binkam</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Lapor Polisi</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Redaksi</button>
										</div>
										<div class="col-md-4 my-3">
											<button class="btn btn-outline-light no-border">Kontak Kami</button>
										</div>
									</div>
								</div>
								<div class="vertical-devider"></div class="vertical-devider">
								<div class="col-md">
									test
								</div>
							</div>
						</div>
					</section>
					<section class="page-footer">
						<div class="footer-copyright text-center py-3">
							<span>© 2015-2019 TRIBRATA NEWS RIAU, All Rights Reserved</span>
						</div>
					</section>
				</footer>';
		return $html;
	}

	public function javascript(){
		$html = '<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
				<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
				<script src="'. base_url() .'assets_frontend/js/carousel.js"></script>
				<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>';
		return $html;
	}
}
