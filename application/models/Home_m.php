<?php


class Home_m extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function kategori($kategori){
		return $this->db
			->select("*")
			->from("berita")
			->where('kategori', $kategori)
			->order_by('tgl_post', 'desc')
			->limit(4, 2)
			->get()->result_array();
	}

	public function get_where($id){
		return $this->db
				->select("*")
				->from("berita")
				->where('id', $id)
				->get()->row();
	}

	public function kategori_one($kategori){
		return $this->db
			->select("*")
			->from("berita")
			->where('kategori', $kategori)
			->order_by('tgl_post', 'desc')
			->get()->row();
	}

	public function terkini(){
		return $this->db
				->select("*")
				->from("berita")
				->order_by('tgl_post', 'desc')
				->limit(4)
				->get()->result_array();
	}

	public function terpopuler(){
		return $this->db
			->select("*")
			->from("berita")
			->order_by('tgl_post', 'desc')
			->limit(6)
			->get()->result_array();
	}

	public function pilihan(){
		return $this->db
			->select("*")
			->from("berita")
			->order_by('views', 'desc')
			->limit(5)
			->get()->result_array();
	}
}
