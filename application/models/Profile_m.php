<?php


class Profile_m extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getWhere($id){
		return $this->db->where('id', $id)->get('profile_tribata')->row();
	}

	public function update($data, $id){
		return $this->db->update('profile_tribata', $data, array('id' => $id));
	}
}
