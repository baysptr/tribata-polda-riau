<?php


class Layout_b extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function meta(){
		$html = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title>Tribata Panel</title>
				<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
				<link rel="stylesheet" href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/css/AdminLTE.min.css">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/css/skins/_all-skins.min.css">
				<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
				<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">';
		return $html;
	}

	public function info(){
		$html = '<header class="main-header">
					<a href="#" class="logo">
						<span class="logo-mini"><b>T</b>RI</span>
						<span class="logo-lg"><b>Tribata</b> Panel</span>
					</a>
					<nav class="navbar navbar-static-top">
						<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
			
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">
								<li class="dropdown user user-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<img src="https://image.flaticon.com/icons/png/512/18/18148.png" class="user-image" alt="User Image">
										<span class="hidden-xs">'. $this->session->userdata['username'] .'</span>
									</a>
									<ul class="dropdown-menu">
										<li class="user-header">
											<img src="https://image.flaticon.com/icons/png/512/18/18148.png" class="img-circle" alt="User Image">
			
											<p>
												'. $this->session->userdata['username'] .'
												<small>Current Login: '. date("d F Y - H:s", strtotime($this->session->userdata['login_date'])) .' WIB</small>
											</p>
										</li>
										<li class="user-footer">
											<div class="pull-left">
												<a href="#" class="btn btn-default btn-flat">Profile</a>
											</div>
											<div class="pull-right">
												<a href="'. site_url() .'welcome/do_logout" class="btn btn-default btn-flat">Sign out</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
				</header>
			
				<aside class="main-sidebar">
					<section class="sidebar">
						<div class="user-panel">
							<div class="pull-left image">
								<img src="https://image.flaticon.com/icons/png/512/18/18148.png" class="img-circle" alt="User Image">
							</div>
							<div class="pull-left info">
								<p>'. $this->session->userdata['username'] .'</p>
								<a href="#"><i class="fa fa-circle text-success"></i>'. date("d F Y H:i", strtotime($this->session->userdata['login_date'])) .' WIB</a>
							</div>
						</div>
						<form action="#" method="get" class="sidebar-form">
							<div class="input-group">
								<input type="text" name="q" class="form-control" placeholder="Search...">
								<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
							</button>
						  </span>
							</div>
						</form>
						<ul class="sidebar-menu" data-widget="tree">
							<li class="header">MAIN NAVIGATION</li>
							<li>
								<a href="'.site_url().'admin">
									<i class="fa fa-th"></i> <span>Dashboard</span>
								</a>
							</li>
							<li class="treeview">
							  <a href="#">
								<i class="fa fa-dashboard"></i> <span>Pengaduan</span>
								<span class="pull-right-container">
								  <i class="fa fa-angle-left pull-right"></i>
								</span>
							  </a>
							  <ul class="treeview-menu">
								<li><a href="'.site_url().'admin/list_pengaduan"><i class="fa fa-circle-o"></i> List Pengaduan</a></li>
								<li><a href="'.site_url().'admin/laporan_darurat"><i class="fa fa-circle-o"></i> Laporan Darurat</a></li>
							  </ul>
							</li>
							<li>
								<a href="'.site_url().'admin/news">
									<i class="fa fa-th"></i> <span>Berita</span>
								</a>
							</li>
							<li>
								<a href="'.site_url().'admin/notif">
									<i class="fa fa-th"></i> <span>Operator Aksi Cepat</span>
								</a>
							</li>
							<li class="treeview">
							  <a href="#">
								<i class="fa fa-dashboard"></i> <span>Profile</span>
								<span class="pull-right-container">
								  <i class="fa fa-angle-left pull-right"></i>
								</span>
							  </a>
							  <ul class="treeview-menu">
								<li><a href="'.site_url().'admin/kata_sambutan"><i class="fa fa-circle-o"></i> Kata Sambutan</a></li>
								<li><a href="'.site_url().'admin/profile"><i class="fa fa-circle-o"></i> Profile</a></li>
								<li><a href="'.site_url().'admin/visi_misi"><i class="fa fa-circle-o"></i> Visi dan Misi</a></li>
							  </ul>
							</li>
						</ul>
					</section>
				</aside>';
		return $html;
	}

	public function footer(){
		$html = '<footer class="main-footer">
					<div class="pull-right hidden-xs">
						<b>Version</b> 2.4.13
					</div>
					<strong>Copyright &copy; 2014-2019 .</strong> All rights
					reserved.
				</footer>';
		return $html;
	}

	public function javascript(){
		$html = '<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/js/adminlte.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/js/demo.js"></script>
				<script>
					$(document).ready(function () {
						$(".sidebar-menu").tree()
					})
				</script>';
		return $html;
	}
}
