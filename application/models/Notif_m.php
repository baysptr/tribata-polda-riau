<?php

class Notif_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        return $this->db
            ->select("*")
            ->from("update_notif")
            ->order_by("tgl_post", "desc")
            ->get()->result_array();
    }

    public function getWhere($id)
    {
        return $this->db->where('id', $id)->get('update_notif')->row();
    }

    public function save($data)
    {
        return $this->db->insert('update_notif', $data);
    }

    public function update($data, $id)
    {
        return $this->db->update('update_notif', $data, array('id' => $id));
    }

    public function hapus($id)
    {
        return $this->db->delete('update_notif', array('id' => $id));
    }
}
