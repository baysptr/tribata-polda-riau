<?php


class Pengaduan_m extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll(){
		return $this->db->get("pengaduan")->result_array();
	}

	public function getWhere($id){
		return $this->db->where('id', $id)->get("pengaduan")->row();
	}

	public function getWhereNomor($nomor){
		return $this->db->where('no_pengaduan', $nomor)->get("pengaduan")->row();
	}

	public function getCount(){
		return $this->db->count_all("pengaduan");
	}

	public function save($data){
		return $this->db->insert("pengaduan", $data);
	}

	public function update($data, $id){
		return $this->db->update("pengaduan", $data, array('id' => $id));
	}

	public function hapus($id){
		return $this->db->delete("pengaduan", array('id' => $id));
	}

	public function saveDarurat($data){
		return $this->db->insert("laporan_darurat", $data);
	}

	public function getAllDarurat(){
		return $this->db->get("laporan_darurat")->result_array();
	}
}
