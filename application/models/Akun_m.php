<?php


class Akun_m extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function login($username, $password){
		return $this->db
				->select("*")
				->from("user_logs")
				->where("username", $username)
				->where("password", $password)
				->get();
	}
}
