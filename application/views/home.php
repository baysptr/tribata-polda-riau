<!DOCTYPE html>
<html lang="en">

<head>
	<!--meta-->
	<?= $meta ?>
</head>

<body>
	<!--header-->
	<?= $header ?>

	<!-- Header Foto  -->
	<section>
		<div class="header-img">
			<!-- <img src="<?= base_url() ?>assets_frontend/img/Header-img.jpg" alt="Header" width="100%"> -->
			<div class="header-color">
				<div class="outer">
					<div class="middle">
						<div class="inner-width text-white text-center">
							<h1 class="font-bold">Mari Amankan Riau Dari Kejahatan</h1>
							<p>Aman, Tertib, Sejahtera</p>
						</div>
						<div class="inner text-white text-center bg-botblue p-4">
							<div class="row">
								<div class="col-md-6">
									<h6>KEADAAN DARURAT</h6>
									<a href="tel:110"><img class="py-3" src="<?= base_url() ?>assets_frontend/img/medsos/phone.png" alt="110"></a>
									<small>
										<p>Polisi, Pemadam Kebakaran, Ambulans dalam keadaan darurat.</p>
									</small>
								</div>
								<div class="col-md-1">
									<div class="vline"></div>
								</div>
								<div class="col-md-5">
									<h6>TOMBOL DARURAT</h6>
									<button type="button" style="background-color: transparent; border: none " data-toggle="modal" data-target="#pilihan"><img class="py-3" src="<?= base_url() ?>assets_frontend/img/medsos/bell.png" alt="panic button"></button>
									<small>
										<p>Untuk bantuan polisi yang tidak mendesak, kunjungi portal komunitas</p>
									</small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Header Foto  -->

	<!-- Berita Terkini -->
	<section class="bg-gray">
		<div class="container custom-container mb-3 pt-3">
			<div class="row justify-content-between">
				<div class="col-4">
					<div class="row">
						<h2 class="title-header">BERITA&nbsp;</h2>
						<h2 class="title-header font-red">TERKINI</h2>
					</div>
				</div>
				<div class="col-4 item-right">
					<button type="button" class="btn btn-primary btn-blue" onclick="window.location='<?= site_url() ?>binkam'">
						<h6 class="font-bold font-white">LIHAT SEMUA BERITA</h6>
					</button>
				</div>
			</div>

			<div class="row d-flex my-3">
				<?php foreach ($terbarus as $terbaru) { ?>
					<div class="col-6 d-flex justify-content-center align-self-center">
						<div class="card mb-3" style="max-width: 540px;">
							<div class="row no-gutters">
								<div class="col-md-4">
									<a href="<?= site_url() ?>read/news/<?= $terbaru['id'] . '/' . url_title($terbaru['judul'], 'dash', true) ?>"><img src="<?= $controller->is_url($terbaru['foto']) ?>" height="200" class="card-img" alt="..."></a>
								</div>
								<div class="col-md-8 d-flex align-items-center">
									<div class="card-body">
										<h5 class="card-title"><a href="<?= site_url() ?>read/news/<?= $terbaru['id'] . '/' . url_title($terbaru['judul'], 'dash', true) ?>" style="color: black"><?= $terbaru['judul'] ?></a> </h5>
										<p class="card-text"><span class="card-text badge badge-danger"><?= $terbaru['kategori'] ?></span><small class="text-muted ml-2"><?= $controller->hari_ini($terbaru['tgl_post']) . ', ' . date('d F Y', strtotime($terbaru['tgl_post'])) . ' ' . date('H:i', strtotime($terbaru['tgl_post'])) ?> WIB</small></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>

			</div>
		</div>
	</section>
	<!-- End Berita Terkini -->

	<!-- Berita Pilihan -->
	<section>
		<div class="container custom-container my-3">
			<div class="row">
				<div class="col-md-3" style="display:inline-block">
					<div class="row">
						<h2 class="title-header">BERITA&nbsp;</h2>
						<h2 class="title-header font-red">PILIHAN</h2>
					</div>
				</div>
				<div class="col-md px-0">
					<div class="underline mx-auto"></div>
				</div>
			</div>
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner row w-100 mx-auto">

					<div class="carousel-item col-md-4 active">
						<div class="card" onclick="window.location='<?= site_url() ?>read/news/<?= $pilihans[0]['id'] . '/' . url_title($pilihans[0]['judul'], 'dash', true) ?>'">
							<img class="card-img-top" src="<?= $controller->is_url($pilihans[0]['foto']) ?>" style="height: 240px; width: 340px" alt="Card image cap">
							<div class="card-img-overlay d-flex align-items-end p-0">
								<div class="berita-pilihan px-3 py-2">
									<h5 class="card-title font-bold" style="color: white"><?= $pilihans[0]['judul'] ?></h5>
								</div>
							</div>
						</div>
					</div>

					<?php for ($i = 2; $i < count($pilihans); $i++) { ?>
						<div class="carousel-item col-md-4">
							<div class="card" onclick="window.location='<?= site_url() ?>read/news/<?= $pilihans[$i]['id'] . '/' . url_title($pilihans[$i]['judul'], 'dash', true) ?>'">
								<img class="card-img-top img-fluid" src="<?= $controller->is_url($pilihans[$i]['foto']) ?>" style="height: 240px; width: 340px" alt="Card image cap">
								<div class="card-img-overlay d-flex align-items-end p-0">
									<div class="berita-pilihan px-3 py-2">
										<h5 class="card-title font-bold" style="color: white"><?= $pilihans[$i]['judul'] ?></h5>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</section>
	<!-- End Berita Pilihan -->

	<!-- Media Social -->
	<!--MEDSOS-->
	<?= $medsos ?>
	<!-- End Media Social -->

	<!--footer-->
	<?= $footer ?>
	<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></button>


	<!-- Modal Pilihan -->
	<div class="modal fade" id="pilihan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" style="min-width: 100%;">
			<div class="modal-content" style="background-color: transparent;">
				<div class="modal-body d-flex justify-content-center">
					<div class="row d-flex justify-content-center">
						<div class="buat mx-4 my-4" style="background-image: url('<?= base_url() ?>assets_frontend/img/lapor/buat.jpg')">
							<div class="header-color d-flex align-items-center justify-content-center btn-modal">
								<button type="button" class="btn btn-orange btn-lg font-bold" id="buatLaporan">Buat
									Laporan</button>
							</div>
						</div>
						<div class="darurat mx-4 my-4" style="background-image: url('<?= base_url() ?>assets_frontend/img/lapor/darurat.jpg')">
							<div class="header-color d-flex align-items-center justify-content-center btn-modal">
								<button type="button" class="btn btn-red btn-lg font-bold" id="laporanDarurat">Laporan
									Darurat</button>
							</div>
						</div>
						<div class="cek mx-4 my-4" style="background-image: url('<?= base_url() ?>assets_frontend/img/lapor/cek.jpg')">
							<div class="header-color d-flex align-items-center justify-content-center btn-modal">
								<button type="button" class="btn btn-blue btn-lg font-bold" id="cekLaporan">Cek
									Laporan</button>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- End Modal Pilihan -->

	<!-- Modal Buat Laporan -->
	<div class="modal fade overflow-man-auto" id="formBuatLaporan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header align-items-center text-center form-buat" style="background-image: url('<?= base_url() ?>/assets_frontend/img/lapor/header-buat.jpg');">
					<div class="header-color d-flex align-items-center" style="width: 100%;">
						<h4 class="modal-title font-bold" id="exampleModalLabel">Buat Laporan</h4>
					</div>
				</div>
				<form action="<?= site_url() ?>pelaporan/do_lapor" method="post">
					<div class="modal-body font-blue ">
						<div class="modal-header mb-3">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<h2>Pengaduan Masyarakat</h2>
						<div class="row">
							<div class="col-md">
								<h4>Identitas Anda</h4>
								<div class="form-group">
									<input type="text" name="identitas_ktp" id="identitas_ktp" class="form-control" placeholder="No. KTP">
								</div>
								<div class="form-group">
									<input type="text" name="identitas_tempat_lahir" id="identitas_tempat_lahir" class="form-control" placeholder="Tempat Lahir">
								</div>
								<div class="form-group">
									<input class="form-control" name="identitas_tgl_lahir" id="tanggalLahir" placeholder="Tanggal Lahir">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="identitas_nama" id="p_nama" placeholder="Nama Lengkap">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="identitas_pekerjaan" id="p_pekerjaan" placeholder="Pekerjaan">
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="3" name="identitas_alamat" id="p_alamat" placeholder="Alamat Lengkap"></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="No. Telepon" name="identitas_tlp">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Email" name="identitas_email">
								</div>
							</div>

							<div class="col-md">
								<h4>Peristiwa Yang Dilaporkan</h4>
								<div class="form-group">
									<input class="form-control" name="peristiwa_tgl" id="waktuKejadian" placeholder="Waktu Kejadian">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="peristiwa_tempat" placeholder="Tempat Kejadian">
								</div>
								<div class="form-group">
									<textarea name="peristiwa" class="form-control" rows="5" placeholder="Kronologi"></textarea>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h4>Siapa Terlapor</h4>
										<div class="form-group">
											<input type="checkbox"> Sama dengan indentitas pengadu
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="terlapor_nama" id="t_nama" placeholder="Nama">
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="terlapor_pekerjaan" id="t_pekerjaan" placeholder="Pekerjaan">
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="terlapor_alamat" id="t_alamat" placeholder="Alamat">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer float-right">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- End Modal Buat Laporan -->

	<!-- Modal Cek Laporan -->
	<div class="modal fade overflow-man-auto" id="formCekLaporan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header align-items-center text-center form-cek" style="background-image: url('<?= base_url() ?>/assets_frontend/img/lapor/header-cek.jpg');">
					<div class="header-color d-flex align-items-center" style="width: 100%;">
						<h4 class="modal-title font-bold">Cek Laporan</h4>
					</div>
				</div>
				<div class="modal-body font-blue ">
					<div class="modal-header mb-3">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<h3 class="text-center">Surat Pemberitahuan Perkembangan Hasil Penyelidikan dan Penyidikan</h3>
					<div class="row">
						<div class="col-md">
							<h4>Identitas Anda</h4>
							<form>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="No. LP">
								</div>
								<button type="button" class="btn btn-primary float-right">Cek</button>
							</form>
						</div>

						<!-- <div class="vline-blue"></div> -->

						<div class="col-md">
							<p>
								Cara Monitoring Perkembangan
								<ol>
									<li>Masukkan Nomor Laporan Polisi
										(Contoh:LPB/02/3/2019/RIAU/RESTA SDA)
									</li>
									<li>Selanjutnya klik Tombol Cek</li>
									<li>Selanjutnya klik Tombol Lihat untuk melihan Dokumen perkembangan laporan</li>
								</ol>
							</p>
						</div>
					</div>
				</div>
				<!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
			</div>
		</div>
	</div>
	<!-- End Modal Cek Laporan -->

	<!-- Modal Laporan Darurat -->
	<div class="modal fade overflow-man-auto" id="formLaporanDarurat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header align-items-center text-center form-darurat" style="background-image: url('<?= base_url() ?>/assets_frontend/img/lapor/header-darurat.jpg');">
					<div class="header-color d-flex align-items-center" style="width: 100%;">
						<h4 class="modal-title font-bold">Laporan Darurat</h4>
					</div>
				</div>
				<div class="modal-body font-blue ">
					<div class="modal-header mb-3">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<h3 class="text-center">Surat Pemberitahuan Laporan Darurat</h3>
					<div class="row">
						<div class="col-md">
							<form action="<?= site_url() ?>pelaporan/do_darurat" method="post">
								<div class="form-group">
									<input type="text" name="nama" class="form-control" placeholder="Nama Pelapor">
								</div>
								<div class="form-group">
									<input type="text" name="no_telp" class="form-control" placeholder="No. Telepon">
								</div>
								<button type="submit" class="btn btn-primary">Lapor</button>
							</form>
						</div>

						<!-- <div class="vline-blue"></div> -->

						<div class="col-md">
							<p>
								Cara Melapor Darurat
								<ol>
									<li>Masukkan Nama Pelapor</li>
									<li>Masukkan Noomor Telepon</li>
									<li>Selanjutnya klik Tombol Lapor</li>
								</ol>
							</p>
						</div>
					</div>
				</div>
				<!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
			</div>
		</div>
	</div>
	<!-- End Modal Laporan Darurat -->


	<!--script-->
	<?= $javascript ?>
	<script>
		//Get the button
		var mybutton = document.getElementById("myBtn");

		// When the user scrolls down 20px from the top of the document, show the button
		window.onscroll = function() {
			scrollFunction()
		};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				mybutton.style.display = "block";
			} else {
				mybutton.style.display = "none";
			}
		}

		// When the user clicks on the button, scroll to the top of the document
		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
	<!-- END To Top Button -->

	<script>
		//Close modal pilihan dari tombol buat laporan
		$("#buatLaporan").on("click", function() {
			$('#pilihan').modal('hide');
		});

		//Open Modal Buat Laporan
		$("#buatLaporan").on("click", function() {
			$('#formBuatLaporan').modal('show');
		});

		//Close modal pilihan dari tombol cek laporan
		$("#cekLaporan").on("click", function() {
			$('#pilihan').modal('hide');
		});

		//Open Modal Cek Laporan
		$("#cekLaporan").on("click", function() {
			$('#formCekLaporan').modal('show');
		});

		//Close modal pilihan dari tombol laporan darurat
		$("#laporanDarurat").on("click", function() {
			$('#pilihan').modal('hide');
		});

		//Open Modal Laporan Darurat
		$("#laporanDarurat").on("click", function() {
			$('#formLaporanDarurat').modal('show');
		});

		//trigger next modal
		// $("#signin").on("click", function () {
		//     $('#myModal2').modal('show');
		// });

		// For Form Date in Modal Buat Laporan
		$('#waktuKejadian').datepicker({
			uiLibrary: 'bootstrap4'
		});
		$('#tanggalLahir').datepicker({
			uiLibrary: 'bootstrap4'
		});
		$('#cekTanggalLahir').datepicker({
			uiLibrary: 'bootstrap4'
		});
		// End For Form Date in Modal Buat Laporan

		$('#nextFormButton').click(function() {
			$('#nextFormButton').fadeOut();
		});

		$('#nextFormButton').click(function() {
			$('#formPeristiwa').fadeIn();
		});

		// $(document).ready(function () {
		//     $("#nextFormButton").click(function () {
		//         $("#nextFormButton").fadeOut();
		//     });
		//     $("#nextFormButton").click(function () {
		//         $("#formPeristiwa").fadeIn();
		//     });
		// });
	</script>

	<!-- Script Scroll Bar -->
	<script type="text/javascript">
		$(document).ready(function() {
			$("#sidebar").mCustomScrollbar({
				theme: "minimal"
			});

			$('#dismiss, .overlay').on('click', function() {
				$('#sidebar').removeClass('active');
				$('.overlay').removeClass('active');
			});

			$('#sidebarCollapse').on('click', function() {
				$('#sidebar').addClass('active');
				$('.overlay').addClass('active');
				$('.collapse.in').toggleClass('in');
				$('a[aria-expanded=true]').attr('aria-expanded', 'false');
			});

			$('input[type="checkbox"]').click(function() {
				if ($(this).prop("checked") == true) {
					$("#t_nama").val($("#p_nama").val());
					$("#t_pekerjaan").val($("#p_pekerjaan").val());
					$("#t_alamat").val($("#p_alamat").val());
				} else if ($(this).prop("checked") == false) {
					$("#t_nama").val("");
					$("#t_pekerjaan").val("");
					$("#t_alamat").val("");
				}
			});
		});
	</script>

</body>

</html>