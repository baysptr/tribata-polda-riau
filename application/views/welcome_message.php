<!DOCTYPE html>
<html lang="en">
<head>
<!--meta-->
	<?= $meta ?>
</head>

<body>
<!--header-->
<?= $header ?>

<!-- Header Foto  -->
<section>
	<div class="header-img">
		<!-- <img src="<?= base_url() ?>assets_frontend/img/Header-img.jpg" alt="Header" width="100%"> -->
		<div class="header-color">
			<div class="outer">
				<div class="middle">
					<div class="inner-width text-white text-center">
						<h1 class="font-bold">Mari Amankan Riau Dari Kejahatan</h1>
						<p>Aman, Tertib, Sejahtera</p>
					</div>
					<div class="inner text-white text-center bg-botblue p-4">
						<div class="row">
							<div class="col-md-6">
								<h6>KEADAAN DARURAT</h6>
								<img class="py-3" src="<?= base_url() ?>assets_frontend/img/medsos/phone.png" alt="110">
								<small>
									<p>Polisi, Pemadam Kebakaran, Ambulans dalam keadaan darurat.</p>
								</small>
							</div>
							<div class="col-md-1">
								<div class=" vline"></div>
							</div>
							<div class="col-md-5">
								<h6>TOMBOL DARURAT</h6>
								<button type="button" style="background-color: transparent; border: none "
										data-toggle="modal" data-target="#pilihan"><img class="py-3"
																						src="<?= base_url() ?>assets_frontend/img/medsos/bell.png" alt="panic button"></button>
								<small>
									<p>Untuk bantuan polisi yang tidak mendesak, kunjungi portal komunitas</p>
								</small>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Header Foto  -->

<!-- Berita Terkini -->
<section class="bg-gray">
	<div class="container custom-container mb-3 pt-3">

		<div class="row justify-content-between">
			<div class="col-4">
				<div class="row">
					<h2 class="title-header">BERITA&nbsp;</h2>
					<h2 class="title-header font-red">TERKINI</h2>
				</div>
			</div>
			<div class="col-4 item-right">
				<button type="button" class="btn btn-primary btn-blue">
					<h6 class="font-bold font-white">LIHAT SEMUA BERITA</h6>
				</button>
			</div>
		</div>

		<div class="row d-flex my-3">
			<div class="col-6 d-flex justify-content-center align-self-center">
				<div class="card mb-3" style="max-width: 540px;">
					<div class="row no-gutters">
						<div class="col-md-4">
							<img src="<?= base_url() ?>assets_frontend//img/img-dashboard/6799116747872-img-20191128-wa0265.jpg"
								 height="200" class="card-img" alt="...">
						</div>
						<div class="col-md-8 d-flex align-items-center">
							<div class="card-body">
								<h5 class="card-title">Kapolres Meranti Bersama Insan Pers Berbincang Dalam
									Coffee
									Morning</h5>
								<!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
								<p class="card-text"><span
										class="card-text badge badge-danger">Humaniora</span><small
										class="text-muted ml-2">Kamis, 28-1-2019 -
										16:52:27
										WIB</small></p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-6 d-flex justify-content-center align-self-center">
				<div class="card mb-3" style="max-width: 540px;">
					<div class="row no-gutters">
						<div class="col-md-4">
							<img src="<?= base_url() ?>assets_frontend/img/img-dashboard/6799116747872-img-20191128-wa0265.jpg" height="200"
								 class="card-img" alt="...">
						</div>
						<div class="col-md-8 d-flex align-items-center">
							<div class="card-body">
								<h5 class="card-title">Kapolres Meranti Bersama Insan Pers Berbincang Dalam
									Coffee
									Morning</h5>
								<!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
								<p class="card-text"><span
										class="card-text badge badge-danger">Humaniora</span><small
										class="text-muted ml-2">Kamis, 28-1-2019 -
										16:52:27
										WIB</small></p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-6 d-flex justify-content-center align-self-center">
				<div class="card mb-3" style="max-width: 540px;">
					<div class="row no-gutters">
						<div class="col-md-4">
							<img src="<?= base_url() ?>assets_frontend//img/img-dashboard/6799116747872-img-20191128-wa0265.jpg"
								 height="200" class="card-img" alt="...">
						</div>
						<div class="col-md-8 d-flex align-items-center">
							<div class="card-body">
								<h5 class="card-title">Kapolres Meranti Bersama Insan Pers Berbincang Dalam
									Coffee
									Morning</h5>
								<!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
								<p class="card-text"><span
										class="card-text badge badge-danger">Humaniora</span><small
										class="text-muted ml-2">Kamis, 28-1-2019 -
										16:52:27
										WIB</small></p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-6 d-flex justify-content-center align-self-center">
				<div class="card mb-3" style="max-width: 540px;">
					<div class="row no-gutters">
						<div class="col-md-4">
							<img src="<?= base_url() ?>assets_frontend//img/img-dashboard/6799116747872-img-20191128-wa0265.jpg"
								 height="200" class="card-img" alt="...">
						</div>
						<div class="col-md-8 d-flex align-items-center">
							<div class="card-body">
								<h5 class="card-title">Kapolres Meranti Bersama Insan Pers Berbincang Dalam
									Coffee
									Morning</h5>
								<!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
								<p class="card-text"><span
										class="card-text badge badge-danger">Humaniora</span><small
										class="text-muted ml-2">Kamis, 28-1-2019 -
										16:52:27
										WIB</small></p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- End Berita Terkini -->

<!-- Berita Pilihan -->
<section>
	<div class="container custom-container my-3">
		<div class="row">
			<div class="col-md-3" style="display:inline-block">
				<div class="row">
					<h2 class="title-header">BERITA&nbsp;</h2>
					<h2 class="title-header font-red">PILIHAN</h2>
				</div>
			</div>
			<div class="col-md px-0">
				<div class="underline mx-auto"></div>
			</div>
		</div>
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner row w-100 mx-auto">
				<div class="carousel-item col-md-4 active">
					<div class="card">
						<img class="card-img-top img-fluid" src="http://placehold.it/800x600/f44242/fff"
							 alt="Card image cap">
						<div class="card-img-overlay d-flex align-items-end p-0">
							<div class="berita-pilihan px-3 py-2">
								<h5 class="card-title font-bold">Kunjungi Wilayah Bengkalis, Kapolda Riau Tinjau
									Kesiapan Polres Bengkalis Dalam</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item col-md-4">
					<div class="card">
						<img class="card-img-top img-fluid" src="http://placehold.it/800x600/418cf4/fff"
							 alt="Card image cap">
						<div class="card-img-overlay d-flex align-items-end p-0">
							<div class="berita-pilihan px-3 py-2">
								<h5 class="card-title font-bold">Kapolres Kampar Pimpin Apel Gelar Pasukan Jelang
									MTQ KE-38 Prov Riau Dan Pilkades</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item col-md-4">
					<div class="card">
						<img class="card-img-top img-fluid" src="http://placehold.it/800x600/3ed846/fff"
							 alt="Card image cap">
						<div class="card-img-overlay d-flex align-items-end p-0">
							<div class="berita-pilihan px-3 py-2">
								<h5 class="card-title font-bold">Kunjungi Wilayah Bengkalis, Kapolda Riau Tinjau
									Kesiapan Polres Bengkalis Dalam</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item col-md-4">
					<div class="card">
						<img class="card-img-top img-fluid" src="http://placehold.it/800x600/42ebf4/fff"
							 alt="Card image cap">
						<div class="card-img-overlay d-flex align-items-end p-0">
							<div class="berita-pilihan px-3 py-2">
								<h5 class="card-title font-bold">Kunjungi Wilayah Bengkalis, Kapolda Riau Tinjau
									Kesiapan Polres Bengkalis Dalam</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item col-md-4">
					<div class="card">
						<img class="card-img-top img-fluid" src="http://placehold.it/800x600/f49b41/fff"
							 alt="Card image cap">
						<div class="card-img-overlay d-flex align-items-end p-0">
							<div class="berita-pilihan px-3 py-2">
								<h5 class="card-title font-bold">Kunjungi Wilayah Bengkalis, Kapolda Riau Tinjau
									Kesiapan Polres Bengkalis Dalam</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item col-md-4">
					<div class="card">
						<img class="card-img-top img-fluid" src="http://placehold.it/800x600/f4f141/fff"
							 alt="Card image cap">
						<div class="card-img-overlay d-flex align-items-end p-0">
							<div class="berita-pilihan px-3 py-2">
								<h5 class="card-title font-bold">Kunjungi Wilayah Bengkalis, Kapolda Riau Tinjau
									Kesiapan Polres Bengkalis Dalam</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item col-md-4">
					<div class="card">
						<img class="card-img-top img-fluid" src="http://placehold.it/800x600/8e41f4/fff"
							 alt="Card image cap">
						<div class="card-img-overlay d-flex align-items-end p-0">
							<div class="berita-pilihan px-3 py-2">
								<h5 class="card-title font-bold">Kunjungi Wilayah Bengkalis, Kapolda Riau Tinjau
									Kesiapan Polres Bengkalis Dalam</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</section>
<!-- End Berita Pilihan -->

<!-- Media Social -->
<!--MEDSOS-->
<?= $medsos ?>
<!-- End Media Social -->

<!--footer-->
<?= $footer ?>
<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></button>

<!--script-->
<?= $javascript ?>

</body>
</html>
