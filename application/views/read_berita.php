<!DOCTYPE html>
<html lang="en">
<head>
	<?= $meta ?>
</head>

<body>
<?= $header ?>

<!-- Content -->
<section class="bg-gray">
	<div class="container custom-container py-4">
		<div class="row">
			<!-- Terbaru / BINKAM -->
			<div class="col-md-8">
				<div class="col-md-12 px-0">
					<h3 class="font-bold"><?= $berita->judul ?></h3>
					<small>
						<p class="text-muted font-bold my-0"><span class="font-red font-bold"><?= $berita->kategori ?></span> -
							<?= $controller->hari_ini($berita->tgl_post).', '.date('d F Y', strtotime($berita->tgl_post)) . ' ' . date('H:i', strtotime($berita->tgl_post)) ?> WIB
						</p>
<!--						<p class=" text-muted">Penulis: <i>DAW</i></p>-->
					</small>

					<div class="card img-detail no-border">
						<img src="<?= $controller->is_url($berita->foto) ?>" alt="Kapolres">
					</div>

					<p align="justify">
						<?= nl2br($berita->isi) ?>
					</p>

					<!-- Komentar -->
					<div class="my-4">
						<h6 class="font-bold font-blue">Komentar Anda:</h6>

						<div class="row">
							<div class="col-md-12">
                                    <textarea cols="30" rows="2" placeholder="Add Comment"
											  class="form-control"></textarea>
								<button style="float:right" class="btn btn-primary mt-3">Add Comment</button>
							</div>
						</div>

						<div class="row">
							<h6 class="font-bold font-blue mr-3 my-auto">Share :</h6>
							<button class="btn btn-secondary mr-2"><i class="fab fa-facebook-f"></i></button>
							<button class="btn btn-secondary mr-2"><i class="fab fa-twitter"></i></button>
							<button class="btn btn-secondary mr-2"><i class="fab fa-instagram"></i></button>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="user-comments">

									<div class="comment">
										<div class="user">Batara Guru <span class="time">2019-12-31</span></div>
										<div class="user-comment"> This is my comment</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- End Komentar -->
				</div>
			</div>
			<!-- End Terbaru / BINKAM -->
			<!-- Terpopuler -->
			<div class="col-md-4">
				<div class="row justify-content-between d-flex justify-content-center align-self-center">
					<h2 class="font-bold red-underline">TERPOPULER</h2>
					<i class="fas fa-angle-right popular-arrow"></i>
				</div>

				<?php foreach ($terpopulers as $terpopuler){ ?>
				<div class="col-md-12 px-0">
					<div class="card my-3">
						<div class="card-body">
							<h6 class="card-title font-bold"><a style="color: black" href="<?= site_url() ?>read/news/<?= $terpopuler['id'] . '/' . url_title($terpopuler['judul'], 'dash', true) ?>"><?= $terpopuler['judul'] ?></a></h6>
							<p class="card-text"><span class="card-text badge badge-danger"><?= $terpopuler['kategori'] ?></span><small
									class="text-muted ml-2"><?= $controller->hari_ini($terpopuler['tgl_post']).', '.date('d F Y', strtotime($terpopuler['tgl_post'])) . ' ' . date('H:i', strtotime($terpopuler['tgl_post'])) ?></small></p>
						</div>
					</div>
				</div>
				<?php } ?>

			</div>
			<!-- End Terpopuler -->
		</div>
	</div>
</section>
<!-- End Content -->

<!-- REKOMENDASI -->
<section>
	<div class="container custom-container py-4">
		<div class="col-md-12">
			<h2 class="font-bold red-underline">REKOMENDASI</h2>
		</div>

		<div class="row">

			<?php foreach ($pilihans as $pilian){ ?>
				<div class="card no-border mx-3 my-3" style="width: 12rem;">
					<a href="<?= site_url() ?>read/news/<?= $pilian['id'] . '/' . url_title($pilian['judul'], 'dash', true) ?>"><img src="<?= $controller->is_url($pilian['foto']) ?>" class="card-img-top" style="width: 210px; height: 160px;" alt="Binkam"></a>
					<div class="card-body px-0 py-0">
						<h5 class="card-title font-bold"><a style="color: black" href="<?= site_url() ?>read/news/<?= $pilian['id'] . '/' . url_title($pilian['judul'], 'dash', true) ?>"><?= $pilian['judul'] ?></a>
						</h5>
					</div>
				</div>
			<?php } ?>

		</div>
	</div>
</section>
<!-- END REKOMENDASI -->

<!-- Media Social -->
<?= $medsos ?>
<!-- End Media Social -->

<?= $footer ?>
<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></button>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
<script src="./assets/js/carousel.js"></script>

<!-- To Top Button -->
<script>
	//Get the button
	var mybutton = document.getElementById("myBtn");

	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function () { scrollFunction() };

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			mybutton.style.display = "block";
		} else {
			mybutton.style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>
<!-- END To Top Button -->

</html>
