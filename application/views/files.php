<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Dokumen Pengaduan";
$obj_pdf->SetTitle($title);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();

$html = '
<h1 align="center">Dokumen Pengaduan Masyarakat</h1><span style="text-align: center">No. Pengaduan : <strong>'.$data['no_pengaduan'].'</strong></span><hr/>
<table width="100%" style="font-size: x-large; padding: 8px">
	<tbody>
		<tr>
			<td colspan="2" align="center"><strong><h3>Identitas Pengadu</h3></strong><hr/></td>
		</tr>
		<tr>
			<td width="50%">
				<label style="font-weight: bold">Nama</label><br/>
				'.$data['identitas_nama_lengkap'].'
			</td>
			<td width="50%">
				<label style="font-weight: bold">No. KTP</label><br/>
				'.$data['identitas_no_ktp'].'
			</td>
		</tr>
		<tr>
			<td width="50%">
				<label style="font-weight: bold">No. Telp</label><br/>
				'.$data['identitas_no_telp'].'
			</td>
			<td width="50%">
				<label style="font-weight: bold">e-Mail</label><br/>
				'.$data['identitas_email'].'
			</td>
		</tr>
		<tr>
			<td width="50%">
				<label style="font-weight: bold">Tempat Lahir</label><br/>
				'.$data['identitas_tempat_lahir'].'
			</td>
			<td width="50%">
				<label style="font-weight: bold">Tanggal lahir</label><br/>
				'.date("d F Y", strtotime($data['identitas_tgl_lahir'])).'
			</td>
		</tr>
		<tr>
			<td width="50%">
				<label style="font-weight: bold">Alamat</label><br/>
				'.$data['identitas_alamat'].'
			</td>
			<td width="50%">
				<label style="font-weight: bold">Pekerjaan</label><br/>
				'.$data['identitas_pekerjaan'].'
			</td>
		</tr><hr/>
		<tr><td colspan="2" align="center"><strong><h3>Peristiwa yang dilaporkan</h3></strong><hr/></td></tr>
		<tr>
			<td width="50%">
				<label style="font-weight: bold">Waktu Kejadian</label><br/>
				'.date("d F Y", strtotime($data['peristiwa_waktu'])).'
			</td>
			<td width="50%">
				<label style="font-weight: bold">Tempat Kejadian</label><br/>
				'.$data['peristiwa_tempat'].'
			</td>
		</tr>
		<tr><td colspan="2" width="100%"><label style="font-weight: bold">Apa yang terjadi</label><br/>'.$data['peristiwa'].'</td></tr><hr/>
		<tr><td colspan="2" align="center"><strong><h3>Terlapor</h3></strong><hr/></td></tr>
		<tr>
			<td width="50%">
				<label style="font-weight: bold">Nama Terlapor</label><br/>
				'.$data['terlapor_nama'].' 
			</td>
			<td width="50%">
				<label style="font-weight: bold">Pekerjaan Terlapor</label><br/>
				'.$data['terlapor_pekerjaan'].'
			</td>
		</tr>
		<tr><td colspan="2" width="100%"><label style="font-weight: bold">Alamat Terlapor</label><br/>'.$data['terlapor_alamat'].'</td></tr><hr/>
		<tr><td colspan="2"></td></tr>
		<tr>
			<td width="50%">
				<label style="font-weight: bold">Status Pengaduan</label><br/>
				<span style="background-color: lawngreen">'.strtoupper($data['status']).'</span>
			</td>
			<td width="50%">
				<label style="font-weight: bold">Tanggal Pengaduan</label><br/>
				'.$data['tgl_post'].'
			</td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr><td colspan="2"></td></tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			<td colspan="2" align="center" style="font-weight: bold"><small>&copy; Dokumen dibuat otomatis atas informasi yang anda berikan.</small></td>
		</tr>
	</tbody>
</table>';

$obj_pdf->writeHTML($html, true, false, true, false, '');
$obj_pdf->Output(FCPATH.'upload/files/'.$data['file_rekap'].'.pdf', 'F');
//$obj_pdf->Output($data['file_rekap'].'.pdf', 'F');

header("Content-type:application/pdf");
header("Content-Disposition:attachment;filename=PENGADUAN.pdf");
readfile(FCPATH.'upload/files/'.$data['file_rekap'].'.pdf');
