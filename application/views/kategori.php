<!DOCTYPE html>
<html lang="en">
<head>
	<?= $meta ?>
	<link rel="stylesheet" href="<?= base_url() ?>assets_frontend/css/binkam.css">
</head>

<body>
<?= $header ?>

<!-- Header Foto -->
<section class="bg-gray">
	<div class="container custom-container my-3 py-4">
		<div class="card text-white">
			<img src="<?= $controller->is_url($one->foto) ?>" class="card-img" alt="Binkam">
			<div class="card-img-overlay d-flex align-items-center" style="background-color: rgba(0, 0, 0, 0.3);">
			</div>
			<div class="card-img-overlay d-flex align-items-end">
				<div style="width: 60%;">
					<h6 class="card-text font-bold font-binkamtag"><?= $one->kategori ?></h6>
					<h2 class="card-title font-bold"><a href="<?= site_url() ?>read/news/<?= $one->id . '/' . url_title($one->judul, 'dash', true) ?>" style="color: white"><?= $one->judul ?></a></h2>
					<p class="card-text"><i class="far fa-clock mx-3"></i><span><?= $controller->hari_ini($one->tgl_post).', '.date('d F Y', strtotime($one->tgl_post)) . ' ' . date('H:i', strtotime($one->tgl_post)) ?></span><i class="far fa-eye mx-3"></i><span><?= $one->views ?></span>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Header Foto -->

<!-- Content -->
<section>
	<div class="container custom-container py-4">
		<div class="row">
			<!-- Terbaru / BINKAM -->
			<div class="col-md-8">
				<div class="col-md-12">
					<h2 class="font-bold"><span class="red-underline">TERBARU/ </span><span
							class="font-red"><?= $one->kategori ?></span></h2>
				</div>

				<?php foreach ($lists as $list){ ?>
				<div class="card no-border my-4">
					<div class=" row no-gutters">
						<div class="col-md-4">
							<a href="<?= site_url() ?>read/news/<?= $list['id'] . '/' . url_title($list['judul'], 'dash', true) ?>"><img src="<?= $controller->is_url($list['foto']) ?>" height="200"
											 class="card-img" alt="..."></a>
						</div>
						<div class="col-md-8 d-flex align-items-center">
							<div class="card-body py-0">
								<a href="<?= site_url() ?>read/news/<?= $list['id'] . '/' . url_title($list['judul'], 'dash', true) ?>" style="color: black"><h5 class="card-title font-bold"><?= $list['judul'] ?></h5></a>
								<p class="card-text"><?= preg_replace('/\s+/', ' ', word_limiter($list['isi'], 20)) ?></p>
								<p class="card-text"><span class="card-text font-red font-bold">NEWS</span><small
										class="text-muted ml-2"><?= $controller->hari_ini($list['tgl_post']).', '.date('d F Y', strtotime($list['tgl_post'])) . ' ' . date('H:i', strtotime($list['tgl_post'])) ?></small></p>
							</div>
						</div>
					</div>
				</div>
				<hr />
				<?php } ?>

				<!-- Pagination -->
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Previous</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
				<!-- End Pagination -->
			</div>
			<!-- End Terbaru / BINKAM -->

			<!-- Terpopuler -->
			<div class="col-md-4">
				<div class="row justify-content-between d-flex justify-content-center align-self-center">
					<h2 class="font-bold red-underline">TERPOPULER</h2>
					<i class="fas fa-angle-right popular-arrow"></i>
				</div>

				<?php foreach ($terkinis as $terkini){ ?>
				<div class="col-md-12 px-0">
					<div class="card my-3">
						<div class="card-body">
							<h6 class="card-title font-bold"><a style="color: black" href="<?= site_url() ?>read/news/<?= $terkini['id'] . '/' . url_title($terkini['judul'], 'dash', true) ?>"><?= $terkini['judul'] ?></a></h6>
							<p class="card-text"><span class="card-text badge badge-danger"><?= $terkini['kategori'] ?></span><small
									class="text-muted ml-2"><?= $controller->hari_ini($terkini['tgl_post']).', '.date('d F Y', strtotime($terkini['tgl_post'])) . ' ' . date('H:i', strtotime($terkini['tgl_post'])) ?></small></p>
						</div>
					</div>
				</div>
				<?php } ?>

			</div>
			<!-- End Terpopuler -->
		</div>
	</div>
</section>
<!-- End Content -->

<!-- Media Social -->
<?= $medsos ?>
<!-- End Media Social -->

<?= $footer ?>
<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></button>

</body>
<?= $javascript; ?>

<script type="text/javascript">
	$(document).ready(function () {
		$("#sidebar").mCustomScrollbar({
			theme: "minimal"
		});

		$('#dismiss, .overlay').on('click', function () {
			$('#sidebar').removeClass('active');
			$('.overlay').removeClass('active');
		});

		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').addClass('active');
			$('.overlay').addClass('active');
			$('.collapse.in').toggleClass('in');
			$('a[aria-expanded=true]').attr('aria-expanded', 'false');
		});
	});
</script>

<!-- To Top Button -->
<script>
	//Get the button
	var mybutton = document.getElementById("myBtn");

	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function () { scrollFunction() };

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			mybutton.style.display = "block";
		} else {
			mybutton.style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>
<!-- END To Top Button -->

</html>
