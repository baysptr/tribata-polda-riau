<!DOCTYPE html>
<html>
<head>
<!--	meta-->
	<?= $meta ?>
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
<!--	info-->
	<?= $info ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Notifier
				<small>Kelolah Operator Aksi Cepat</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Notifier</a></li>
<!--				<li class="active">Blank page</li>-->
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">List Akun Telegram</h3>
					<div class="box-tools pull-right">
						<div class="btn btn-sm btn-primary" onclick="tambahData()"><span class="fa fa-plus-square"></span> Tambah</div>
						<div class="btn btn-sm btn-warning" onclick="opInfo()"><span class="fa fa-info"></span> Info</div>
					</div>
				</div>
				<div class="box-body">
					<table class="table table-striped table-hover">
						<thead class="table-info">
							<tr>
								<td><strong>NO</strong></td>
								<td><strong>NAMA</strong></td>
								<td><strong>ID TELEGRAM</strong></td>
								<td><strong>TANGGAL INPUT</strong></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($notifs as $notif){ ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $notif['nama'] ?></td>
									<td><?= $notif['id_telegram'] ?></td>
									<td><?= date("d F Y H:i", strtotime($notif['tgl_post'])) ?></td>
									<td><div onclick="ambilData('<?= $notif['id'] ?>')" class="btn btn-sm btn-block btn-info"><span class="fa fa-search"></span> Detail</div></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
<!--				<div class="box-footer">-->
<!--					Footer-->
<!--				</div>-->
			</div>
		</section>
	</div>
	<!-- /.content-wrapper -->

<!--	footer-->
	<?= $footer ?>
</div>
<!-- ./wrapper -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
      </div>
      <form action="<?= site_url() ?>admin/do_notif" method="POST">
      	<input type="hidden" name="id" id="id">
      	<div class="modal-body">
        	<div class="form-group">
        		<label>Nama</label>
        		<input type="text" name="nama" id="nama" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>ID Telegram</label>
        		<input type="text" name="telegram" id="telegram" class="form-control">
        	</div>
	    </div>
	    <div class="modal-footer">
	    	<button type="button" class="btn btn-warning" onclick="hapusData($('#id').val())">Hapus</button>
	        <button type="submit" class="btn btn-primary" id="btnAksi">Save changes</button>
	        <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
	    </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Info</h5>
      </div>
      	<input type="hidden" name="id" id="id">
      	<div class="modal-body">
        	<h3>cara menemukan ID Telegram</h3>
        	<ul>
        		<li>Buka aplikasi telegram anda</li>
        		<li>cari @kwkw_bot</li>
        		<li>lalu tuliskan pada kolom chat <strong>!id</strong></li>
        		<li>lalu bot tersebut, akan mengirimkan id telegram anda</li>
        	</ul>
	    </div>
	    <div class="modal-footer">
	    	<button type="button" class="btn btn-warning" onclick="opClose()">Tutup</button>
	    </div>
    </div>
  </div>
</div>

<!--javascript-->
<?= $javascript ?>

<script type="text/javascript">
	function opInfo(){
		$("#modalInfo").modal("show");
	}

	function opClose(){
		$("#modalInfo").modal("hide");
	}

	function hapusData(id){
		if (confirm("Apakah anda yakin untuk hapus data ini?")===true) {
			$.get("<?= site_url() ?>admin/do_delete_notif/"+id, function(data){
				location.reload();
			})
		}
	}

	function tambahData(){
		$("#exampleModalLabel").html("Form Tambah Operator");
		$("#btnAksi").html("Tambah");
		$("#exampleModal").modal("show");
	}

	function closeModal(){
		$("#nama").val("");
		$("#telegram").val("");
		$("#exampleModal").modal("hide");
	}

	function ambilData(id){
		$.get("<?= site_url() ?>admin/get_notif/"+id, function(data){
			notif = JSON.parse(data);
			$("#id").val(notif.id);
			$("#nama").val(notif.nama);
			$("#telegram").val(notif.id_telegram);
			$("#exampleModalLabel").html("Form Ubah Data");
			$("#btnAksi").html("Ubah Data!");
			$("#exampleModal").modal("show");
		});
	}
</script>

</body>
</html>
