<!DOCTYPE html>
<html>
<head>
<!--	meta-->
	<?= $meta ?>
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
<!--	info-->
	<?= $info ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Pengaduan
				<small>Laporan Pengaduan Masyarakat</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Pengaduan</a></li>
<!--				<li class="active">Blank page</li>-->
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">List Pengaduan Masyarakat</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
								title="Collapse">
							<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<table class="table table-striped table-hover">
						<thead class="table-info">
							<tr>
								<td><strong>NO</strong></td>
								<td><strong>NO PENGADUAN</strong></td>
								<td><strong>WAKTU KEJADIAN</strong></td>
								<td><strong>WAKTU PENGADUAN</strong></td>
								<td><strong>NAMA PENGADU</strong></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($pengaduans as $pengaduan){ ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $pengaduan['no_pengaduan'] ?></td>
									<td><?= $pengaduan['peristiwa_waktu'] ?></td>
									<td><?= $pengaduan['tgl_post'] ?></td>
									<td><?= $pengaduan['identitas_nama_lengkap'] ?></td>
									<td><a href="/tribata_web/upload/files/<?= $pengaduan['file_rekap'] ?>.pdf" download="<?= $pengaduan['file_rekap'] ?>.pdf" target="_blank" class="btn btn-sm btn-block btn-info"><span class="fa fa-search"></span> File</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
<!--				<div class="box-footer">-->
<!--					Footer-->
<!--				</div>-->
			</div>
		</section>
	</div>
	<!-- /.content-wrapper -->

<!--	footer-->
	<?= $footer ?>
</div>
<!-- ./wrapper -->

<!--javascript-->
<?= $javascript ?>
</body>
</html>
