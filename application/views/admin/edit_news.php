<!DOCTYPE html>
<html>
<head>
<!--	meta-->
	<?= $meta ?>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
<!--	info-->
	<?= $info ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Control Content Berita
				<small>Tribata Polda Riau</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Control Content</a></li>
<!--				<li class="active">Blank page</li>-->
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ubah Berita</h3>
					<div class="box-tools pull-right">
						<div onclick="window.location='<?= site_url() ?>admin/news'" class="btn btn-sm btn-primary"><span class="fa fa-arrow-left"></span> Kembali ke berita</div>
					</div>
				</div>
				<div class="box-body">
					<form action="<?= site_url() ?>admin/do_update_new" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_id" id="_id" value="<?= $new->id ?>">
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" id="title" name="title" value="<?= $new->judul ?>">
						</div>
						<div class="form-group">
							<label>Category</label>
							<select name="kategori" id="kategori" class="form-control select2" style="width: 100%">
								<optgroup label="Direktorat">
									<option <?= ($new->kategori == "BINKAM") ? "selected" : "" ?> value="BINKAM">BINKAM</option>
									<option <?= ($new->kategori == "BINMAS") ? "selected" : "" ?> value="BINMAS">BINMAS</option>
									<option <?= ($new->kategori == "SABHARA") ? "selected" : "" ?> value="SABHARA">SABHARA</option>
									<option <?= ($new->kategori == "BRIMOB") ? "selected" : "" ?> value="BRIMOB">BRIMOB</option>
									<option <?= ($new->kategori == "POLAIR") ? "selected" : "" ?> value="POLAIR">POLAIR</option>
								</optgroup>
								<optgroup label="Reskrim">
									<option <?= ($new->kategori == "RESKRIM-UM") ? "selected" : "" ?> value="RESKRIM-UM">RESKRIM UM</option>
									<option <?= ($new->kategori == "RESKRIM-SUS") ? "selected" : "" ?> value="RESKRIM-SUS">RESKRIM SUS</option>
									<option <?= ($new->kategori == "RES-NARKOBA") ? "selected" : "" ?> value="RES-NARKOBA">RES NARKOBA</option>
									<option <?= ($new->kategori == "RES-CYBER-MEDIA") ? "selected" : "" ?> value="RES-CYBER-MEDIA">RES CYBER MEDIA</option>
								</optgroup>
								<option <?= ($new->kategori == "LANTAS") ? "selected" : "" ?> value="LANTAS">LANTAS</option>
								<option <?= ($new->kategori == "MITRA-POLISI") ? "selected" : "" ?> value="MITRA-POLISI">MITRA POLISI</option>
								<optgroup label="Giat">
									<option <?= ($new->kategori == "GIAT-OPS") ? "selected" : "" ?> value="GIAT-OPS">GIAT OPS</option>
									<option <?= ($new->kategori == "GIAT-ADMINISTRASI") ? "selected" : "" ?> value="GIAT-ADMINISTRASI">GIAT ADMINISTRASI</option>
								</optgroup>
							</select>
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea name="isi" id="isi" rows="10" cols="80" class="form-control"><?= $new->isi ?></textarea>
						</div>
						<div class="form-group" align="center" id="display_foto">
							<img src="<?= $controller->is_url($new->foto) ?>" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);" width="100%">
						</div>
						<div class="form-group">
							<label>Image</label>
							<input type="file" name="foto" id="foto" class="form-control" placeholder="Click here to upload foto">
							<small><input type="checkbox" onclick="myFunction()" id="change_foto" name="change_foto"> Centang untuk konfirmasi mengganti foto</small>
						</div>
						<div class="form-group">
							<label>Tanggal Post</label>
							<input type="text" name="tgl_post" id="tgl_post" class="form-control" value="<?= $new->tgl_post ?>">
						</div>
						<div class="form-group" align="right">
							<button type="submit" class="btn btn-primary"><span class="fa fa-edit"></span> Ubah</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
	<!-- /.content-wrapper -->

<!--	footer-->
	<?= $footer ?>
</div>
<!-- ./wrapper -->

<!--javascript-->
<?= $javascript ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/id.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script type="text/javascript">
	$(function () {
		$('.select2').select2();
		$('#tgl_post').datetimepicker({
			locale: 'id',
			format: 'YYYY-MM-DD HH:mm',
			viewMode: 'years'
		});
		CKEDITOR.replace( 'isi' );
	});

	function myFunction() {
		var checkBox = document.getElementById("change_foto");
		var text = document.getElementById("display_foto");
		if (checkBox.checked == true){
			text.style.display = "none";
		} else {
			text.style.display = "block";
		}
	}
</script>
</body>
</html>
