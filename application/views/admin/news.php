<!DOCTYPE html>
<html>
<head>
<!--	meta-->
	<?= $meta ?>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
<!--	info-->
	<?= $info ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Control Content Berita
				<small>Tribata Polda Riau</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Control Content</a></li>
<!--				<li class="active">Blank page</li>-->
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">List Berita</h3>
					<div class="box-tools pull-right">
						<div onclick="addBerita()" class="btn btn-sm btn-primary"><span class="fa fa-plus-square"></span> Tambah Berita</div>
					</div>
				</div>
				<div class="box-body">
					<table id="myTable" class="table table-bordered table-striped" width="100%">
						<thead class="table-active">
							<tr>
								<td><strong>TITLE</strong></td>
								<td><strong>CATEGORY</strong></td>
								<td><strong>CONTENT</strong></td>
								<td><strong>IMAGE</strong></td>
								<td></td>
								<td></td>
<!--								<td colspan="2" align="center">#</td>-->
							</tr>
						</thead>
						<tbody>
							<?php foreach ($news as $new){ ?>
								<tr>
									<td><?= $new['judul'] ?></td>
									<td><?= $new['kategori'] ?></td>
									<td><?= preg_replace('/\s+/', ' ', word_limiter($new['isi'], 10)) ?></td>
									<td width="20%"><img src="<?= $controller->is_url($new['foto']) ?>" width="100%"></td>
									<td><div onclick="window.location='<?= site_url() ?>admin/edit_news/<?= $new['id'] ?>'" class="btn btn-block btn-sm btn-warning"><span class="fa fa-edit"></span> Edit</div></td>
									<td><div onclick="deleteBerita('<?= $new['id'] ?>')" class="btn btn-block btn-sm btn-danger"><span class="fa fa-trash"></span> Delete</div></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	<!-- /.content-wrapper -->

<!--	footer-->
	<?= $footer ?>
</div>
<!-- ./wrapper -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="closeBerita()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Form Tambah Berita</h4>
			</div>
			<div class="modal-body">
				<form action="<?= site_url() ?>admin/do_save_news" method="post" enctype="multipart/form-data" id="myForm">
					<div class="form-group">
						<label>Title</label>
						<input type="text" class="form-control" id="title" name="title" placeholder="Title Content">
					</div>
					<div class="form-group">
						<label>Category</label>
						<select name="kategori" id="kategori" class="form-control select2" style="width: 100%">
							<option value="default" disabled selected>-- Pilih Kategori Berita --</option>
							<optgroup label="Direktorat">
								<option value="BINKAM">BINKAM</option>
								<option value="BINMAS">BINMAS</option>
								<option value="SABHARA">SABHARA</option>
								<option value="BRIMOB">BRIMOB</option>
								<option value="POLAIR">POLAIR</option>
							</optgroup>
							<optgroup label="Reskrim">
								<option value="RESKRIM-UM">RESKRIM UM</option>
								<option value="RESKRIM-SUS">RESKRIM SUS</option>
								<option value="RES-NARKOBA">RES NARKOBA</option>
								<option value="RES-CYBER-MEDIA">RES CYBER MEDIA</option>
							</optgroup>
							<option value="LANTAS">LANTAS</option>
							<option value="MITRA-POLISI">MITRA POLISI</option>
							<optgroup label="Giat">
								<option value="GIAT-OPS">GIAT OPS</option>
								<option value="GIAT-ADMINISTRASI">GIAT ADMINISTRASI</option>
							</optgroup>
						</select>
					</div>
					<div class="form-group">
						<label>Content</label>
						<textarea name="isi" id="isi" rows="10" cols="80" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label>Image</label>
						<input type="file" name="foto" id="foto" class="form-control" placeholder="Click here to upload foto">
					</div>
					<div class="form-group">
						<label>Tanggal Post</label>
						<input type="text" name="tgl_post" id="tgl_post" class="form-control" placeholder="Click here to get date">
					</div>
					<div class="form-group">
						<div style="text-align: right">
							<button type="submit" class="btn btn-sm btn-primary"><span class="fa fa-save"></span> Save!</button>&nbsp;
							<button type="button" class="btn btn-default" onclick="closeBerita()"><span class="fa fa-close"></span> Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!--javascript-->
<?= $javascript ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/id.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script type="text/javascript">
	$(function () {
		$("#myTable").DataTable({
			"ordering": false
		});
		$('.select2').select2();
		$('#tgl_post').datetimepicker({
			locale: 'id',
			format: 'YYYY-MM-DD HH:mm',
			viewMode: 'years'
		});
		CKEDITOR.replace( 'isi' );
	});

	function deleteBerita(id) {
		Swal.fire({
			title: 'Benarkah anda ingin menghapus data ini?',
			text: "Jika anda yakin, maka pada halaman utama akan terhapus juga!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				$.post("<?= site_url() ?>admin/do_delete_news", { _id: id }, function (data) {
					Swal.fire(
						'Deleted!',
						'Data anda telah terhapus!',
						'success'
					);
					location.reload();
				})
			}
		})
	}

	function closeBerita() {
		document.getElementById('myForm').reset();
		$("#kategori").val("default");
		$("#isi").val("");
		$("#myModal").modal("hide");
	}

	function addBerita() {
		$("#myModal").modal("show");
	}
</script>
</body>
</html>
