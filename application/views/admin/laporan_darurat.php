<!DOCTYPE html>
<html>
<head>
<!--	meta-->
	<?= $meta ?>
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
<!--	info-->
	<?= $info ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Pengaduan
				<small>Laporan Pengaduan Darurat</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Pengaduan Darurat</a></li>
<!--				<li class="active">Blank page</li>-->
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">List Pengaduan Darurat</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
								title="Collapse">
							<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<table class="table table-striped table-hover">
						<thead class="table-info">
							<tr>
								<td><strong>NO</strong></td>
								<td><strong>NAMA PENGADU</strong></td>
								<td><strong>NO TELP</strong></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($darurats as $darurat){ ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $darurat['nama_lengkap'] ?></td>
									<td><?= $darurat['no_telp'] ?></td>
									<td><div class="btn btn-sm btn-block btn-info"><span class="fa fa-search"></span> File</div></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
<!--				<div class="box-footer">-->
<!--					Footer-->
<!--				</div>-->
			</div>
		</section>
	</div>
	<!-- /.content-wrapper -->

<!--	footer-->
	<?= $footer ?>
</div>
<!-- ./wrapper -->

<!--javascript-->
<?= $javascript ?>
</body>
</html>
