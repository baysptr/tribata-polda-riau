<!DOCTYPE html>
<html>
<head>
<!--	meta-->
	<?= $meta ?>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
<!--	info-->
	<?= $info ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Control Content Profile
				<small>Tribata Polda Riau</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Control Content</a></li>
<!--				<li class="active">Blank page</li>-->
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Visi dan Misi</h3>
				</div>
				<div class="box-body">
					<form action="<?= site_url() ?>admin/do_visi_misi" method="post">
						<div class="form-group">
							<textarea id="isi" name="isi" class="form-group" rows="20" cols="80"><?= $sambutan->visi_misi ?></textarea>
						</div>
						<div class="form-group" align="right">
							<button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
	<!-- /.content-wrapper -->

<!--	footer-->
	<?= $footer ?>
</div>
<!-- ./wrapper -->

<!--javascript-->
<?= $javascript ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/id.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace( 'isi', {
			height: 600
		});
	});
</script>
</body>
</html>
