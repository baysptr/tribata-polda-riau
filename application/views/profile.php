<!DOCTYPE html>
<html lang="en">
<head>
	<?= $meta ?>
	<link rel="stylesheet" href="<?= base_url() ?>assets_frontend/css/binkam.css">
</head>

<body>
<?= $header ?>

<!-- Content -->
<section>
	<div class="container custom-container py-4">
		<div class="row">
			<!-- Terbaru / BINKAM -->
			<div class="col-md-8 bg-gray">
				<div class="col-md-12">
					<h2 class="font-bold"><span class="red-underline">Profile / </span><span
							class="font-red"> Profile</span></h2>
				</div>
				<div class="col-md-12">
					<?= $profile->profile ?>
				</div>
			</div>
			<!-- End Terbaru / BINKAM -->

			<!-- Terpopuler -->
			<div class="col-md-4">
				<div class="col-md-12">
					<div class="row justify-content-between d-flex justify-content-center align-self-center">
						<h2 class="font-bold red-underline">TERPOPULER</h2>
						<i class="fas fa-angle-right popular-arrow"></i>
					</div>
				</div>

				<?php foreach ($terkinis as $terkini){ ?>
				<div class="col-md-12 px-0">
					<div class="card my-3">
						<div class="card-body">
							<h6 class="card-title font-bold"><a style="color: black" href="<?= site_url() ?>read/news/<?= $terkini['id'] . '/' . url_title($terkini['judul'], 'dash', true) ?>"><?= $terkini['judul'] ?></a></h6>
							<p class="card-text"><span class="card-text badge badge-danger"><?= $terkini['kategori'] ?></span><small
									class="text-muted ml-2"><?= $controller->hari_ini($terkini['tgl_post']).', '.date('d F Y', strtotime($terkini['tgl_post'])) . ' ' . date('H:i', strtotime($terkini['tgl_post'])) ?></small></p>
						</div>
					</div>
				</div>
				<?php } ?>

			</div>
			<!-- End Terpopuler -->
		</div>
	</div>
</section>
<!-- End Content -->

<!-- Media Social -->
<?= $medsos ?>
<!-- End Media Social -->

<?= $footer ?>
<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></button>

</body>
<?= $javascript; ?>

<script type="text/javascript">
	$(document).ready(function () {
		$("#sidebar").mCustomScrollbar({
			theme: "minimal"
		});

		$('#dismiss, .overlay').on('click', function () {
			$('#sidebar').removeClass('active');
			$('.overlay').removeClass('active');
		});

		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').addClass('active');
			$('.overlay').addClass('active');
			$('.collapse.in').toggleClass('in');
			$('a[aria-expanded=true]').attr('aria-expanded', 'false');
		});
	});
</script>

<!-- To Top Button -->
<script>
	//Get the button
	var mybutton = document.getElementById("myBtn");

	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function () { scrollFunction() };

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			mybutton.style.display = "block";
		} else {
			mybutton.style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>
<!-- END To Top Button -->

</html>
