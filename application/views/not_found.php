<!DOCTYPE html>
<html lang="en">
<head>
	<?= $meta ?>
</head>

<body>
<?= $header ?>

<!-- Content -->
<section class="bg-gray">
	<div class="container custom-container py-4">
		<div class="row">
			<!-- Terbaru / BINKAM -->
			<div class="col-md-12">
				<h1 style="text-align: center;">Page is Not Found<br/>505</h1>
			</div>
			<!-- End Terpopuler -->
		</div>
	</div>
</section>
<!-- End Content -->

<!-- REKOMENDASI -->
<section>
	<div class="container custom-container py-4">
		<div class="col-md-12">
			<h2 class="font-bold red-underline">REKOMENDASI</h2>
		</div>

		<div class="row">

			<?php foreach ($pilihans as $pilian){ ?>
			<div class="card no-border mx-3 my-3" style="width: 12rem;">
				<img src="<?= $pilian['foto'] ?>" class="card-img-top" style="width: 210px; height: 160px;" alt="Binkam">
				<div class="card-body px-0 py-0">
					<h5 class="card-title font-bold">Kapolres Meranti Bersama Insan Pers Berbincang Dalam Coffee
					</h5>
				</div>
			</div>
			<?php } ?>

		</div>
	</div>
</section>
<!-- END REKOMENDASI -->

<!-- Media Social -->
<?= $medsos ?>
<!-- End Media Social -->

<?= $footer ?>
<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></button>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
<script src="./assets/js/carousel.js"></script>

<!-- To Top Button -->
<script>
	//Get the button
	var mybutton = document.getElementById("myBtn");

	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function () { scrollFunction() };

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			mybutton.style.display = "block";
		} else {
			mybutton.style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>
<!-- END To Top Button -->

</html>
