<?php


class Res_cyber_media extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Layout_f');
		$this->load->model('Home_m');
	}

	public function index()
	{
		$data['meta'] = $this->Layout_f->meta();
		$data['medsos'] = $this->Layout_f->medsos();
		$data['footer'] = $this->Layout_f->footer();
		$data['javascript'] = $this->Layout_f->javascript();
		$data['lists'] = $this->Home_m->kategori("RES-CYBER-MEDIA");
		$data['one'] = $this->Home_m->kategori_one("RES-CYBER-MEDIA");
		$data['terkinis'] = $this->Home_m->terpopuler();
		$data['header'] = $this->Layout_f->header($this->Home_m->terkini());
		$data['controller'] = $this;
		$this->load->view('kategori', $data);
	}

	public function is_url($uri){
		if(preg_match( '/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$uri)){
			return $uri;
		}
		else{
			return base_url().'upload/news/'.$uri;
		}
	}

	function hari_ini($d){
		$hari = date("D", strtotime($d));

		switch($hari){
			case 'Sun':
				$hari_ini = "Minggu";
				break;

			case 'Mon':
				$hari_ini = "Senin";
				break;

			case 'Tue':
				$hari_ini = "Selasa";
				break;

			case 'Wed':
				$hari_ini = "Rabu";
				break;

			case 'Thu':
				$hari_ini = "Kamis";
				break;

			case 'Fri':
				$hari_ini = "Jumat";
				break;

			case 'Sat':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;

	}
}
