<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Layout_f');
		$this->load->model('Home_m');
	}

	public function index()
	{
		$data['meta'] = $this->Layout_f->meta();
		$data['medsos'] = $this->Layout_f->medsos();
		$data['footer'] = $this->Layout_f->footer();
		$data['javascript'] = $this->Layout_f->javascript();
		$data['terbarus'] = $this->Home_m->terkini();
		$data['pilihans'] = $this->Home_m->pilihan();
		$data['header'] = $this->Layout_f->header($this->Home_m->terkini());
		$data['controller'] = $this;
		$this->load->view('home', $data);
	}

	public function login()
	{
		$this->load->view('admin/login');
	}

	public function do_login()
	{
		$this->load->model("Akun_m");
		$username = htmlentities($this->input->post('username'));
		$password = md5($this->input->post('password'));

		$query = $this->Akun_m->login($username, $password);
		if ($query->num_rows() < 1) {
			echo "<script>alert('Maaf akses anda tidak kami ketahui');window.location='" . site_url() . "welcome/login'</script>";
		} else {
			$row = $query->row();
			$data = array(
				"_id" => $row->id,
				"username" => $row->username,
				"level_user" => $row->level_user,
				"status_log" => 1,
				"login_date" => date("Y-m-d H:i:s"),
			);
			$this->session->set_userdata($data);
			if ($row->level_user == "admin") {
				redirect(site_url() . 'admin');
			} else {
				redirect(site_url() . 'operator');
			}
		}
	}

	public function do_logout()
	{
		$this->session->sess_destroy();
		redirect(site_url() . 'welcome/login');
	}

	public function test()
	{
		$this->do_telegram('430265248', 'test andika');
	}

	public function do_telegram($id, $pesan)
	{
		$url = "https://api.telegram.org/bot816456554:AAFMeZmizXsdAtc8VvqEKLu_DuXz7N7fcwI/sendMessage?parse_mode=html&chat_id=" . $id;
		$url = $url . "&text=" . urlencode($pesan);
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public function is_url($uri)
	{
		if (preg_match('/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $uri)) {
			return $uri;
		} else {
			return base_url() . 'upload/news/' . $uri;
		}
	}

	function hari_ini($d)
	{
		$hari = date("D", strtotime($d));

		switch ($hari) {
			case 'Sun':
				$hari_ini = "Minggu";
				break;

			case 'Mon':
				$hari_ini = "Senin";
				break;

			case 'Tue':
				$hari_ini = "Selasa";
				break;

			case 'Wed':
				$hari_ini = "Rabu";
				break;

			case 'Thu':
				$hari_ini = "Kamis";
				break;

			case 'Fri':
				$hari_ini = "Jumat";
				break;

			case 'Sat':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;
	}
}
