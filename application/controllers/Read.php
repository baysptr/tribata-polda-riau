<?php


class Read extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Layout_f');
		$this->load->model('Home_m');
	}

	public function index()
	{
		$data['meta'] = $this->Layout_f->meta();
		$data['medsos'] = $this->Layout_f->medsos();
		$data['footer'] = $this->Layout_f->footer();
		$data['javascript'] = $this->Layout_f->javascript();
		$data['terbarus'] = $this->Home_m->terkini();
		$data['pilihans'] = $this->Home_m->pilihan();
		$data['header'] = $this->Layout_f->header($this->Home_m->terkini());
		$this->load->view('not_found', $data);
	}

	public function news()
	{
		$this->load->model("News_m");
		$id = $this->uri->segment('3');
		$data['meta'] = $this->Layout_f->meta();
		$data['medsos'] = $this->Layout_f->medsos();
		$data['footer'] = $this->Layout_f->footer();
		$data['javascript'] = $this->Layout_f->javascript();
		$data['terpopulers'] = $this->Home_m->terpopuler();
		$data['pilihans'] = $this->Home_m->pilihan();
		$data['header'] = $this->Layout_f->header($this->Home_m->terkini());
		$data['controller'] = $this;
		if (!is_numeric($id)){
			$this->load->view('not_found', $data);
		}else{
			$data['berita'] = $this->Home_m->get_where($id);
			$this->News_m->read($id);
			$this->load->view('read_berita', $data);
		}
	}

	public function is_url($uri){
		if(preg_match( '/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$uri)){
			return $uri;
		}
		else{
			return base_url().'upload/news/'.$uri;
		}
	}

	function hari_ini($d){
		$hari = date("D", strtotime($d));

		switch($hari){
			case 'Sun':
				$hari_ini = "Minggu";
				break;

			case 'Mon':
				$hari_ini = "Senin";
				break;

			case 'Tue':
				$hari_ini = "Selasa";
				break;

			case 'Wed':
				$hari_ini = "Rabu";
				break;

			case 'Thu':
				$hari_ini = "Kamis";
				break;

			case 'Fri':
				$hari_ini = "Jumat";
				break;

			case 'Sat':
				$hari_ini = "Sabtu";
				break;

			default:
				$hari_ini = "Tidak di ketahui";
				break;
		}

		return $hari_ini;

	}
}
