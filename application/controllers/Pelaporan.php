<?php

date_default_timezone_set("Asia/Jakarta");
class Pelaporan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pengaduan_m');
		$this->load->model('Notif_m');
	}

	public function do_darurat()
	{
		$op = $this->Notif_m->getAll();
		$data = array(
			"nama_lengkap" => strtoupper($this->input->post("nama")),
			"no_telp" => $this->input->post("no_telp"),
			"tgl_post" => date("Y-m-d H:i:s")
		);
		if ($this->Pengaduan_m->saveDarurat($data)) {
			for($i=0;$i<count($op);$i++){
				$this->do_telegram($op[$i]['id_telegram'], "Hallo!, ada pengaduan masyarakat baru yang bersifat darurat dengan nomor telp: ".$data['no_telp']." silahkan anda check pada panel web.");
			}
			echo "<script>alert('Terimakasih atas informasinya, sebentar lagi petugas kami akan menghubungi anda');window.location='" . base_url() . "';</script>";
		} else {
			echo "<script>alert('Maaf data anda tidak bisa kami proses, pastikan informasi yang anda masukan benar');window.location='" . base_url() . "';</script>";
		}
	}

	public function do_lapor()
	{
		$op = $this->Notif_m->getAll();
		$no_urut = $this->Pengaduan_m->getCount() + 1;
		$inp_user = array(
			"no_pengaduan" => $this->generate_no_pengaduan(),
			"identitas_no_ktp" => $this->input->post('identitas_ktp'),
			"identitas_tempat_lahir" => strtoupper($this->input->post('identitas_tempat_lahir')),
			"identitas_tgl_lahir" => $this->input->post('identitas_tgl_lahir'),
			"identitas_nama_lengkap" => strtoupper($this->input->post('identitas_nama')),
			"identitas_pekerjaan" => strtoupper($this->input->post('identitas_pekerjaan')),
			"identitas_alamat" => strtoupper($this->input->post("identitas_alamat")),
			"identitas_no_telp" => $this->input->post('identitas_tlp'),
			"identitas_email" => strtoupper($this->input->post('identitas_email')),
			"peristiwa_waktu" => date("Y-m-d", strtotime($this->input->post('peristiwa_tgl'))),
			"peristiwa_tempat" => strtoupper($this->input->post("peristiwa_tempat")),
			"peristiwa" => strtoupper($this->input->post('peristiwa')),
			"terlapor_nama" => strtoupper($this->input->post('terlapor_nama')),
			"terlapor_pekerjaan" => strtoupper($this->input->post('terlapor_pekerjaan')),
			"terlapor_alamat" => strtoupper($this->input->post('terlapor_alamat')),
			"status" => 'diterima',
			"file_rekap" => uniqid("PENGADUAN_") . $no_urut,
			"tgl_post" => date("Y-m-d H:i:s")
		);
		if ($this->Pengaduan_m->save($inp_user)) {
			for($i=0;$i<count($op);$i++){
				$this->do_telegram($op[$i]['id_telegram'], "Hallo!, ada pengaduan masyarakat baru dengan nomor: ".$inp_user['no_pengaduan']." silahkan anda check pada panel web.");
			}

			$data['data'] = $inp_user;
			$this->load->helper('pdf');

			$this->load->view('files', $data);
		} else {
			echo "<script>alert('Maaf sistem tidak bisa memproses, pastikan semua data benar dan semua kolom telah di isi semua');window.location='" . base_url() . "';</script>";
		}
	}

	public function do_telegram($id, $pesan)
	{
		$url = "https://api.telegram.org/bot816456554:AAFMeZmizXsdAtc8VvqEKLu_DuXz7N7fcwI/sendMessage?parse_mode=html&chat_id=" . $id;
		$url = $url . "&text=" . urlencode($pesan);
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public function generate_no_pengaduan()
	{
		$urutan = $this->Pengaduan_m->getCount() + 1;
		$seg1 = date("d") . '.' . $urutan;
		$seg2 = date("m");
		$seg3 = $this->KonDecRomawi(date("Y"));
		$nomor = 'LPB/' . $seg1 . '/' . $seg2 . '/' . $seg3 . '/RIAU/RESTA SDA';
		return $nomor;
	}

	function KonDecRomawi($angka)
	{
		$hsl = "";
		if ($angka < 1 || $angka > 3999) {
			$hsl = "Batas Angka 1 s/d 3999";
		} else {
			while ($angka >= 1000) {
				$hsl .= "M";
				$angka -= 1000;
			}
			if ($angka >= 500) {
				if ($angka > 500) {
					if ($angka >= 900) {
						$hsl .= "M";
						$angka -= 900;
					} else {
						$hsl .= "D";
						$angka -= 500;
					}
				}
			}
			while ($angka >= 100) {
				if ($angka >= 400) {
					$hsl .= "CD";
					$angka -= 400;
				} else {
					$angka -= 100;
				}
			}
			if ($angka >= 50) {
				if ($angka >= 90) {
					$hsl .= "XC";
					$angka -= 90;
				} else {
					$hsl .= "L";
					$angka -= 50;
				}
			}
			while ($angka >= 10) {
				if ($angka >= 40) {
					$hsl .= "XL";
					$angka -= 40;
				} else {
					$hsl .= "X";
					$angka -= 10;
				}
			}
			if ($angka >= 5) {
				if ($angka == 9) {
					$hsl .= "IX";
					$angka -= 9;
				} else {
					$hsl .= "V";
					$angka -= 5;
				}
			}
			while ($angka >= 1) {
				if ($angka == 4) {
					$hsl .= "IV";
					$angka -= 4;
				} else {
					$hsl .= "I";
					$angka -= 1;
				}
			}
		}
		return ($hsl);
	}
}
