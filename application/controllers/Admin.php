<?php

date_default_timezone_set("Asia/jakarta");

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('status_log')){
			echo "<script>alert('Maaf kami tidak mengenali hak akses anda');window.location='".site_url()."welcome/login';</script>";
		}
		$this->load->model("Layout_b");
		$this->load->model("News_m");
		$this->load->model("Profile_m");
		$this->load->model("Notif_m");
	}

	public function index(){
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$this->load->view('admin/home', $data);
	}

	public function news(){
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['news'] = $this->News_m->getAll();
		$data['controller'] = $this;
		$this->load->view('admin/news', $data);
	}

	public function edit_news($id){
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['new'] = $this->News_m->getWhere($id);
		$data['controller'] = $this;
		$this->load->view('admin/edit_news', $data);
	}

	public function hapus_file($id){
		$data = $this->News_m->getWhere($id);
		if(file_exists("./upload/news/".$data->foto)) {
			unlink("./upload/news/".$data->foto);
		}
	}

	public function do_update_new(){
		$id = $this->input->post("_id");
		$check_foto = $this->input->post('change_foto');
		if($check_foto){
			$this->hapus_file($id);
			$data = array(
				"judul" => $this->input->post('title'),
				"kategori" => $this->input->post('kategori'),
				"isi" => $this->input->post('isi'),
				"foto" => $this->_uploadImage('foto'),
				"tgl_post" => $this->input->post("tgl_post")
			);
			if($this->News_m->update($data, $id)){
				echo "<script>alert('Data telah diubah');window.location='".site_url()."admin/news'</script>";
			}else{
				echo "<script>alert('Maaf data anda belum tersimpan');window.location='".site_url()."admin/news'</script>";
			}
		}else{
			$data = array(
				"judul" => $this->input->post('title'),
				"kategori" => $this->input->post('kategori'),
				"isi" => $this->input->post('isi'),
				"tgl_post" => $this->input->post("tgl_post")
			);
			if($this->News_m->update($data, $id)){
				echo "<script>alert('Data telah diubah');window.location='".site_url()."admin/news'</script>";
			}else{
				echo "<script>alert('Maaf data anda belum tersimpan');window.location='".site_url()."admin/news'</script>";
			}
		}
	}

	public function do_save_news(){
		$data = array(
			"judul" => $this->input->post('title'),
			"kategori" => $this->input->post('kategori'),
			"isi" => $this->input->post('isi'),
			"foto" => $this->_uploadImage('foto'),
			"tgl_post" => $this->input->post("tgl_post"),
			"views" => 0
		);
		if($this->News_m->save($data)){
			echo "<script>window.location='".site_url()."admin/news'</script>";
		}else{
			echo "<script>alert('Maaf data anda belum tersimpan');window.location='".site_url()."admin/news'</script>";
		}
	}

	public function do_delete_news(){
		$id = $this->input->post("_id");
		$this->News_m->hapus($id);
		return "Success";
	}

	private function _uploadImage($var)
	{
		$config['upload_path']          = './upload/news/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = uniqid("FILE_");
		$config['overwrite']			= true;
		$config['max_size']             = 2048; // 2MB

		$this->load->library('upload', $config);

		if ($this->upload->do_upload($var)) {
			return $this->upload->data("file_name");
		}

		return "default.jpg";
	}

	public function is_url($uri){
		if(preg_match( '/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$uri)){
			return $uri;
		}
		else{
			return base_url().'upload/news/'.$uri;
		}
	}

	public function kata_sambutan(){
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['sambutan'] = $this->Profile_m->getWhere('1');
		$this->load->view('admin/kata_sambutan', $data);
	}

	public function do_katasambutan(){
		$data = array(
			"kata_sambutan" => $this->input->post('isi'),
		);
		if($this->Profile_m->update($data, '1')){
			echo "<script>alert('Data berhasil diubah');window.location='".site_url()."admin/kata_sambutan';</script>";
		}else{
			echo "<script>alert('Data gagal diubah');window.location='".site_url()."admin/kata_sambutan';</script>";
		}
	}

	public function profile(){
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['sambutan'] = $this->Profile_m->getWhere('1');
		$this->load->view('admin/profile', $data);
	}

	public function do_profile(){
		$data = array(
			"profile" => $this->input->post('isi'),
		);
		if($this->Profile_m->update($data, '1')){
			echo "<script>alert('Data berhasil diubah');window.location='".site_url()."admin/profile';</script>";
		}else{
			echo "<script>alert('Data gagal diubah');window.location='".site_url()."admin/profile';</script>";
		}
	}

	public function visi_misi(){
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['sambutan'] = $this->Profile_m->getWhere('1');
		$this->load->view('admin/visi_misi', $data);
	}

	public function do_visi_misi(){
		$data = array(
			"visi_misi" => $this->input->post('isi'),
		);
		if($this->Profile_m->update($data, '1')){
			echo "<script>alert('Data berhasil diubah');window.location='".site_url()."admin/visi_misi';</script>";
		}else{
			echo "<script>alert('Data gagal diubah');window.location='".site_url()."admin/visi_misi';</script>";
		}
	}

	public function list_pengaduan(){
		$this->load->model("Pengaduan_m");
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['pengaduans'] = $this->Pengaduan_m->getAll();
		$this->load->view('admin/list_pengaduan', $data);
	}

	public function laporan_darurat(){
		$this->load->model("Pengaduan_m");
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['darurats'] = $this->Pengaduan_m->getAllDarurat();
		$this->load->view('admin/laporan_darurat', $data);
	}

	public function notif(){
		$this->load->model("Pengaduan_m");
		$data['meta'] = $this->Layout_b->meta();
		$data['info'] = $this->Layout_b->info();
		$data['footer'] = $this->Layout_b->footer();
		$data['javascript'] = $this->Layout_b->javascript();
		$data['notifs'] = $this->Notif_m->getAll();
		$this->load->view('admin/notif', $data);
	}

	public function do_notif(){
		$id = $this->input->post('id');
		$data = array(
			"nama" => $this->input->post('nama'),
			"id_telegram" => $this->input->post('telegram'),
			"tgl_post" => date("Y-m-d H:i:s")
		);
		if($id == null || $id == ""){
			if($this->Notif_m->save($data)){
				echo "<script>window.location='".site_url()."admin/notif';</script>";
			}else{
				echo "<script>alert('Maaf data yang anda masukan tidak bisa kami proses');window.location='".site_url()."admin/notif';</script>";
			}
		}else{
			if($this->Notif_m->update($data, $id)){
				echo "<script>window.location='".site_url()."admin/notif';</script>";
			}else{
				echo "<script>alert('Maaf data yang anda masukan tidak bisa kami proses');window.location='".site_url()."admin/notif';</script>";
			}
		}
	}

	public function get_notif($id){
		$data = $this->Notif_m->getWhere($id);
		echo json_encode($data);
	}

	public function do_delete_notif($id){
		if($this->Notif_m->hapus($id)){
			echo "<script>window.location='".site_url()."admin/notif';</script>";
		}else{
			echo "<script>alert('Maaf data yang anda masukan tidak bisa kami proses');window.location='".site_url()."admin/notif';</script>";
		}
	}
}
