# Tribata riau webase

## fitur
- Manage content berita
- Manage content profile
- Manage laporan pengaduan masyarakat
- Dinamis content
- Future User Interface

## kebutuhan
- php7.2 and all dependecy
- apache2
- mysql-server

## cara install
- clone this repository to root directory web server
- create database using name _tribata_
- import file _initial_tribata.sql_ to database _tribata_
- change _base_url_ on file _config.php_ in directory _application > config_ line number 26, to your DNS
- change credentials or user pass database on file _database.php_ in directory _application > config_
- change your permissions on this project to 777 or 775